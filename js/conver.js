const btnCalcular = document.getElementById('btnCalcular');
const btnRegistrar = document.getElementById('btnRegistrar');
const btnBorrar = document.getElementById('btnBorrar');
const selOrigen = document.getElementById('origen');
const selDestino = document.getElementById('destino');

btnCalcular.addEventListener('click',function(){

    //codificar el boton de calcular
    //obtener valores de los inputs texts
    let cantidad = document.getElementById('cantidad').value;
    let origen = document.getElementById('origen').value;
    let destino = document.getElementById('destino').value;
    let parrafo = document.getElementById('registros');

    let subtotal=0;
    let comision=1.03;
    let total=0;
    let commision = 0;

    //realiza los calculos
    peso=19.25;
    dolarc=1.35;
    euro=.99;



    switch(parseInt(origen)){
        case 1: //peso mex
            switch(parseInt(destino)){
                case 1: //pesomex
                    subtotal = cantidad;
                    break;
                case 2://peso mex a dolar americano
                    subtotal = cantidad/peso;
                    break;
                case 3://peso mex a dolar canadine
                    subtotal = (cantidad/peso) * dolarc;
                    break;
                case 4://peso mex a euro
                    subtotal = (cantidad/peso) * euro;
                    break;
            }
            break;
        case 2: //dolar americano
            switch(parseInt(destino)){
                case 1: //dolar americano a peso mex
                    subtotal = cantidad * peso;
                    break;
                case 2://dolar americano
                    subtotal = cantidad;
                    break;
                case 3://dolar americano a dolar canadine
                    subtotal = cantidad * dolarc;
                    break;
                case 4://dolar americano a euro
                    subtotal = cantidad * euro;
                    break;
            }
            break;
        case 3: //dolar canadine
            switch(parseInt(destino)){
                case 1: //dolar canadine a peso mex
                    subtotal = (cantidad/dolarc) * peso;
                    break;
                case 2://dolar canadine a dolar americano
                    subtotal = cantidad/dolarc;
                    break;
                case 3://dolar canadine
                    subtotal = cantidad;
                    break;
                case 4://dolar canadine a euro
                    subtotal = (cantidad/dolarc) * euro;
                    break;
            }
            break;
        case 4: //euro
            switch(parseInt(destino)){
                case 1: //euro a peso mex
                    subtotal = (cantidad/euro) * peso;
                    break;
                case 2://euro a dolar americano
                    subtotal = cantidad/euro;
                    break;
                case 3://euro a dolar canadine
                    subtotal = (cantidad/euro) * dolarc;
                    break;
                case 4://euro
                    subtotal = cantidad;
                    break;
            }
            break;
    }
    commision = subtotal * 0.03;

    total = subtotal * comision; // Agrega un 3% al subtotal

    document.getElementById('subtotal').value = subtotal;
    document.getElementById('comision').value = commision;
    document.getElementById('totalPagar').value = total;
    

});

let sumaTotal = 0;

btnRegistrar.addEventListener('click', function() {
    // Obtener los valores calculados
    let subtotal = document.getElementById('subtotal').value;
    let origen = document.getElementById('origen');
    let total = document.getElementById('totalPagar').value;
    let destino = document.getElementById('destino');
    let cantidad = document.getElementById('cantidad').value;
  
    let tipoMonedaOrigen = origen.options[origen.selectedIndex].innerHTML;
    let tipoMonedaDestino = destino.options[destino.selectedIndex].innerHTML;
  
    // Agregar los valores al contenido del párrafo
    registros.innerHTML += "Se cambio: " + tipoMonedaOrigen + " $" + cantidad + "<br>";
    registros.innerHTML += "a " + tipoMonedaDestino + " $" + total + "<br>";

    // Calcular la tasa de cambio correspondiente
  let tasaCambio = 1;
  if (tipoMonedaDestino === "Peso mexicano") {
    tasaCambio = 19.85;
  } else if (tipoMonedaDestino === "Dólar canadiense") {
    tasaCambio = 1.35;
  } else if (tipoMonedaDestino === "Euro") {
    tasaCambio = 0.99;
  }

  // Calcular el total en dólares americanos
  let totalUSD = parseFloat(total) * tasaCambio;
  
    // Actualizar la suma total
  sumaTotal += totalUSD;

  
   // Actualizar el contenido del párrafo "total"
  document.getElementById("total").innerHTML = "Suma total en dólares: $" + sumaTotal;
  });

  btnBorrar.addEventListener('click',function(){
    document.getElementById('cantidad').value = '';
    document.getElementById('origen').value = '';
    document.getElementById('destino').value = '';
    document.getElementById('registros').innerHTML = '';
    document.getElementById('subtotal').value = '';
    document.getElementById('comision').value = '';
    document.getElementById('totalPagar').value = '';
    subtotal=0;
    //codificar el boton de borrar

});

selOrigen.addEventListener('change', function() {
    const origen = selOrigen.value;
    const destino = selDestino.value;

    // Desactivar la opción seleccionada en el select de destino y en el select de origen
    const opcionesDestino = selDestino.querySelectorAll('option');
    const opcionesOrigen = selOrigen.querySelectorAll('option');
    opcionesDestino.forEach((opcion) => {
        if (opcion.value === origen && opcion.value !== destino) {
            opcion.disabled = true;
        } else {
            opcion.disabled = false;
        }
    });
    opcionesOrigen.forEach((opcion) => {
        if (opcion.value === destino && opcion.value !== origen) {
            opcion.disabled = true;
        } else {
            opcion.disabled = false;
        }
    });

    // Reactivar la opción previamente desactivada en el select de destino y en el select de origen
    selDestino.querySelector(`option[value="${destino}"]`).disabled = false;
    selOrigen.querySelector(`option[value="${origen}"]`).disabled = false;
});

selDestino.addEventListener('change', function() {
    const origen = selOrigen.value;
    const destino = selDestino.value;

    // Desactivar la opción seleccionada en el select de origen y en el select de destino
    const opcionesDestino = selDestino.querySelectorAll('option');
    const opcionesOrigen = selOrigen.querySelectorAll('option');
    opcionesOrigen.forEach((opcion) => {
        if (opcion.value === destino && opcion.value !== origen) {
            opcion.disabled = true;
        } else {
            opcion.disabled = false;
        }
    });
    opcionesDestino.forEach((opcion) => {
        if (opcion.value === origen && opcion.value !== destino) {
            opcion.disabled = true;
        } else {
            opcion.disabled = false;
        }
    });

    // Reactivar la opción previamente desactivada en el select de origen y en el select de destino
    selOrigen.querySelector(`option[value="${origen}"]`).disabled = false;
    selDestino.querySelector(`option[value="${destino}"]`).disabled = false;
});